# README #

This is a sample Android application project 

Functionality:
allow user to specify a URL, then show web-side HTML code

ButterKnife View Injection was used for the UI Elements

Testing: 
RobolectricGradleTestRunner and JUnit

Testing AsyncTask required special care:
AsyncTask runs the task in a background thread.
The issue that is presented, is the tests must be run on the main thread.

It's necessary to wait for the AsyncTask to finish before performing to the subsequent test or to quit testing routine. runTestOnUiThread method allows conducting test case on the main thread. Listener classes: CallbackListener and DownloadListener were added using the callbacks (onFail and onSuccess) are used to assert that a condition holds or doesn't hold. A real world implementation will use the callbacks to provide useful functionality like to activate notifications perhaps.

Activity lifecycle tested using myActivityAppearsAsExpectedInitially, testRotation and testRestoreSaveInstanceState test methods

Notes:
I had considered using SearchView for getting the user input, since a button was included to 
get text rather CR, I chose to use EditText