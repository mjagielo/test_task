package com.inspirethis.mike.viewhtml;

import java.util.concurrent.CountDownLatch;

/**
 * Created by mike on 6/23/15.
 * starts decrementing the countdown() method and the lock is removed.
 * This allows the result of the AsyncTask to be tested.
 * The lock is automatically removed after 15 seconds by the signal.await() call.
 */
public abstract class CallbackListener {
    private CountDownLatch signal;

    CallbackListener(CountDownLatch signal) {
        this.signal = signal;
    }

    public void onSuccess(String result) {
        signal.countDown();
    }

    public void onFail() {
        signal.countDown();
    }
}