package com.inspirethis.mike.viewhtml;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @InjectView(R.id.editText)
    EditText mEditText;

    @InjectView(R.id.button)
    Button btnShow;

    @InjectView(R.id.html_textview)
    TextView tvHTML;

    protected String mHTMLString;
    private String mHTMLKey = "html_string";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null || !savedInstanceState.containsKey(mHTMLKey)) {
            mHTMLString = "";
        } else {
            mHTMLString = savedInstanceState.getString(mHTMLKey);
            Log.d("onCreate: ", mHTMLString);
        }

        setContentView(R.layout.activity_main);

        // inject UI Components
        ButterKnife.inject(this);
        // resets any previously queried HTML result or empty string
        tvHTML.setText(mHTMLString);

        tvHTML.setTextColor(Color.MAGENTA);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mHTMLString != null && !mHTMLString.equals(""))
            outState.putString(mHTMLKey, mHTMLString);
        Log.d("onSaveInstanceState: ", mHTMLString);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_main);
        // resets view with previously queried data
        tvHTML.setText(mHTMLString);
    }

    @OnClick(R.id.button)
    public void showHTML() {

        // clear any pre-existing text being shown from previous queries
        if (tvHTML.getText().toString() != null && tvHTML.getText().toString().length() > 0)
            tvHTML.setText("");

        // get entry from editText removing any trailing whitespaces
        String url = mEditText.getText().toString().trim();

        if (isNetworkConnected()) {
            if (url != null && isValidUrl(url)) {
                // disable button while performing task
                btnShow.setEnabled(false);
                initDownLoadTask(url);
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_valid_url), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection_needed), Toast.LENGTH_SHORT).show();
        }
    }

    protected boolean isNetworkConnected() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    protected void initDownLoadTask(String url) {
        final CountDownLatch signal = new CountDownLatch(1);
        DownLoadHTMLTask task = new DownLoadHTMLTask(new DownloadListener(signal));
        task.execute(url);
    }

    protected boolean isValidUrl(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url);
        if (m.matches())
            return true;
        else
            return false;
    }

    protected void hideKeyboard() {
        // Check if no view has focus:
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class DownLoadHTMLTask extends AsyncTask<String, Void, String> {
        private final String LOG_TAG = DownLoadHTMLTask.class.getSimpleName();
        private DownloadListener listener;

        public DownLoadHTMLTask(DownloadListener listener) {
            this.listener = listener;
        }

        @Override
        protected String doInBackground(String... params) {

            String response = null;

            try {
                URL url = new URL(params[0]);
                URLConnection connection = url.openConnection();
                // set connection timeout for 3 seconds to avoid long delay where urls are well-formed but not available
                connection.setConnectTimeout(3000);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String input;
                while ((input = bufferedReader.readLine()) != null) {
                    response += input;
                }
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // returns null response if Exception occurs
            return response;
        }

        @Override
        protected void onPostExecute(String str) {
            super.onPostExecute(str);

            // successful query has resulted
            if (str != null && !str.equals("")) {
                tvHTML.setText(str);
                // save the HTML result in instance variable
                mHTMLString = str;
                hideKeyboard();
                listener.onSuccess(str);
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.results_null_check_entry), Toast.LENGTH_SHORT).show();
                listener.onFail();
                // reset input String
                mHTMLString = "";
            }


            // re-enable button when task is complete
            btnShow.setEnabled(true);
        }
    }
}
