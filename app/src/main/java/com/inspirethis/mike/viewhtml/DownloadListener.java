package com.inspirethis.mike.viewhtml;

import android.util.Log;

import junit.framework.Assert;

import java.util.concurrent.CountDownLatch;

/**
 * Created by mike on 6/22/15.
 */
public class DownloadListener extends CallbackListener {
    private final String LOG_TAG = DownloadListener.class.getSimpleName();

    DownloadListener(CountDownLatch signal) {
        super(signal);
    }

    public void onSuccess(String result) {
        Log.d(LOG_TAG, " Success with DownLoadHTMLTask result");
        Assert.assertTrue(true); // for test purposes
        super.onSuccess(result);

    }

    public void onFail() {
        Log.d(LOG_TAG, " Failure with DownLoadHTMLTask result");
        super.onFail();

    }

}
