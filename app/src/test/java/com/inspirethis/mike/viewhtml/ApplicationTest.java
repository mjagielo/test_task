package com.inspirethis.mike.viewhtml;


import android.content.pm.ActivityInfo;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class ApplicationTest {
    private final String LOG_TAG = ApplicationTest.class.getSimpleName();

    MainActivity mActivity;
    CallbackListener mCallbackListener;

    private String mURL = "https://github.com";

    @Before
    public void setUp() throws Exception {
        mActivity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .get();
    }

    @After
    public void tearDown() throws Exception {
        Robolectric.reset();
        mActivity = null;
    }

    @Test
    public void myActivityAppearsAsExpectedInitially() {
        Assert.assertNotNull(mActivity);
    }

    @Test
    public void testRotation() {
        // initial orientation is portrait by default
        if (mActivity.getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
            mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            try {
                // rotation is executed asynchronously, a short wait may be required as a safeguard for change to complete
                Thread.sleep(50);
            } catch (InterruptedException e) {
                Log.d(LOG_TAG, mActivity.getResources().getString(R.string.interrupted_exception));
            }
            Assert.assertTrue(mActivity.getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

            // lifecycle test, set new Activity is not null after rotation
            Assert.assertNotNull(mActivity);
        }
    }

    @Test
    public void testUIElements() throws Exception {
        EditText editText = (EditText) mActivity.findViewById(R.id.editText);
        Button btnShow = (Button) mActivity.findViewById(R.id.button);
        TextView tvHTML = (TextView) mActivity.findViewById(R.id.html_textview);

        Assert.assertNotNull(mActivity.getResources().getString(R.string.edit_text_test_comment), editText);
        Assert.assertNotNull(mActivity.getResources().getString(R.string.button_show_test_comment), btnShow);
        Assert.assertNotNull(mActivity.getResources().getString(R.string.textview_html_test_comment), tvHTML);
    }

    @Test
    public void testIsNetworkConnected() throws Exception {
        Assert.assertTrue(mActivity.isNetworkConnected());
    }

    @Test
    public void testClearHTML() throws Exception {
        mActivity.tvHTML.setText(mActivity.getResources().getString(R.string.sample_text));
        mActivity.showHTML();
        Assert.assertEquals(mActivity.getResources().getString(R.string.text_empty_string), "", mActivity.tvHTML.getText());
    }

    @Test
    public void testGetUrl() throws Exception {
        mActivity.mEditText.setText(mURL);
        mActivity.showHTML();
        Assert.assertEquals(mActivity.getResources().getString(R.string.expect_url), mURL, mActivity.mEditText.getText().toString());
    }

    @Test
    public void testIsValidUrl() {
        Assert.assertTrue(mActivity.isValidUrl(mURL));
    }

    // credit to http://mobilengineering.blogspot.com/2012/05/tdd-testing-asynctasks-on-android.html?m=1
    @Test
    public void testDownloadHTML() {

        final CountDownLatch signal = new CountDownLatch(1);
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                mActivity.initDownLoadTask(mURL);
            }
        });
        try {
            signal.await(15, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Log.d(LOG_TAG, mActivity.getResources().getString(R.string.interrupted_exception));
        }
    }

    @Test
    public void testHideKeyBoard() {
        mActivity.hideKeyboard();
        Assert.assertFalse(isKeyboardShown(mActivity.mEditText.getRootView()));
    }

    @Test
    public void testRestoreSaveInstanceState() {
        Bundle savedInstanceState = new Bundle();
        savedInstanceState.putString(mActivity.getResources().getString(R.string.html_key),
                mActivity.getResources().getString(R.string.html_value));

        mActivity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .restoreInstanceState(savedInstanceState)
                .get();
        Assert.assertEquals(mActivity.getResources().getString(R.string.restore_state_message),
                mActivity.getResources().getString(R.string.html_value),
                savedInstanceState.getString(mActivity.getResources().getString(R.string.html_key)));
    }


    // for test purposes only. credit to StackOverflow solution:
    // http://stackoverflow.com/questions/4745988/how-do-i-detect-if-software-keyboard-is-visible-on-android-device
    protected boolean isKeyboardShown(View rootView) {
    /* 128dp = 32dp * 4, minimum button height 32dp and generic 4 rows soft keyboard */
        final int SOFT_KEYBOARD_HEIGHT_DP_THRESHOLD = 128;

        Rect r = new Rect();
        rootView.getWindowVisibleDisplayFrame(r);
        DisplayMetrics dm = rootView.getResources().getDisplayMetrics();
        int heightDiff = rootView.getBottom() - r.bottom;

        return heightDiff > SOFT_KEYBOARD_HEIGHT_DP_THRESHOLD * dm.density;
    }
}

